Quake III did a cool square root, this is the C++20 version with constexpr and I think no UB.

To use, just add `QuickRSqrt` as an `target_link_libraries` in cmake.